package com.diptika.babajob.babajobassignment;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * Created by diptika.s on 15/05/16.
 */


public class ParseApplication extends Application {
    public final String APPLICATION_ID = "APPLICATION_ID";
    public final String CLIENT_KEY = "***REDACTED***";
    public final String parseServerIP = "http://192.168.1.5:1337/parse/";

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Parse.initialize(new Parse.Configuration.Builder(getBaseContext())
                .applicationId(APPLICATION_ID)
                .clientKey(CLIENT_KEY)
                .server(parseServerIP)
                .build());
    }
}
